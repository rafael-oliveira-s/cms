@extends('layouts.index')
@section('title', 'Home')
@section('content')
<main>

    <div class="content">
        <div class="topBanner">
            <img src="/images/banner_uta.png" alt="">
        </div>

        <div class="title m-b-md">
            Week Highlight
        </div>

        <div class="flex-center">
            <iframe src="https://www.youtube.com/embed/biXZOD61DM4" frameborder="0"
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
            </iframe>
        </div>

        <!-- View mode -->
        <div class="view-mode">
            <div>
                <select data-select-mode>
                    <option value=""> Select the Lyrics View </option>
                    <option value="japanese"> 日本語歌詞 </option>
                    <option value="romaji"> Romaji </option>
                </select>
                <i class="fa fa-caret-down"></i>
            </div>
        </div>
        <!-- /View Mode -->

        <div class="lyrics" id="japanese-lyrics">

            
                
                
            <h3>コトノハノオモイ</h3> <a href="#" class="lyrics__hover">歌詞</a>
            <h4>歌手: <a href="#">井上苑子</a></h4>
            <h4>関連作: <a href="#">川柳少女</a></h4>
            <div>
                言葉はらり<br>
                気持ちふわり<br>
                のせる想いたち<br><br>


                恋に染まってく<br><br>


                君といたら<br>
                雨の中も違った雫になる<br>
                どんな文字を綴ったら<br>
                色褪せずに残るのでしょう？<br>
                知ってるんだ<br>
                いまの二人は遠い　あの日になること<br>
                そうでしょ　神様<br>
                丸まる水も　来ない電車も<br>
                もっと　この瞬間が<br>
                どうか永遠に続いて<br>
                なんて無理かな？<br>
            </div>
        </div>

        <div class="lyrics" id="romaji-lyrics">
            <h3>Kotonoha no Omoi Lyrics</h3>
            <h4>Artist: <a href="#">Sonoko Inoue</a></h4>
            <h4>Tie-in: <a href="#">Senryuu Shoujo OP (Senryu Girl OP)</a></h4>
            <div>
                kotoba harari <br>
                kimochi fuwari <br>
                noseru omoitachi <br><br>


                koi ni somatteku <br><br>


                kimi to itara <br>
                ame no naka mo chigatta shizuku ni naru <br>
                donna moji wo tsuzuttara <br>
                iroasezu ni nokoru no deshou? <br>
                shitterun da <br>
                ima no futari wa tooi ano hi ni naru koto <br>
                sou desho kami-sama <br>
                marumaru mizu mo konai densha mo <br>
                motto kono shunkan ga <br>
                douka eien ni tsuzuite <br>
                nante muri ka na? <br>
            </div>
        </div>
    </div>
    </div>
</main>
@endsection
