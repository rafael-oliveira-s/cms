function myFunction() {
    var menu = document.getElementById('myLinks');
    if (menu.style.display === 'flex') {
        menu.style.display = 'none';
    } else {
        menu.style.display = 'flex';
    }
}

var menuToggleIcon = document.querySelector('[data-toggle-menu]');

if (menuToggleIcon !== null) {
    menuToggleIcon.addEventListener('click', myFunction);
} else {
    console.warn('Cannot locate `data-toggle-menu` target');
}
